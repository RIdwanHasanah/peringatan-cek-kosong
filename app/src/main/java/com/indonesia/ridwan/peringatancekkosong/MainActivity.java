package com.indonesia.ridwan.peringatancekkosong;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       final EditText input = (EditText) findViewById(R.id.edit);
       Button check = (Button) findViewById(R.id.btn);

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(input.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Input Text Kosong...Tolong Masukan Text", Toast.LENGTH_SHORT).show();
                }
                else {

                    Toast.makeText(MainActivity.this, "Input Text Tidak Kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
